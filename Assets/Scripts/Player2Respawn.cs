﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player2Respawn : PlayerRespawn {

	void Awake(){
		kcup = KeyCode.UpArrow;
		kcdown = KeyCode.DownArrow;
		kcleft = KeyCode.LeftArrow;
		kcright = KeyCode.RightArrow;
		kcattack1 = KeyCode.Comma;
		kcattack2 = KeyCode.Period;
		kcattack3 = KeyCode.Slash;
	}
}
