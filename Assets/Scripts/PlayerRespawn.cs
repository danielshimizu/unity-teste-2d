﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerRespawn : MonoBehaviour {

	public GameObject player;

	public Dictionary<string,KeyCode> keys;

	public KeyCode kcup = KeyCode.W;
	public KeyCode kcdown = KeyCode.S;
	public KeyCode kcleft = KeyCode.A;
	public KeyCode kcright = KeyCode.D;
	public KeyCode kcattack1 = KeyCode.H;
	public KeyCode kcattack2 = KeyCode.J;
	public KeyCode kcattack3 = KeyCode.K;

	public int initialLifes = 3;

	public GameObject instance;

	int currentLifes;

	// Use this for initialization
	void Start () {
		currentLifes = initialLifes;
		respawn ();
	}
	
	// Update is called once per frame
	void Update () {
		// Verifica se instance ainda existe, se não existir e ainda houver mais vidas, nasce de novo
		if (instance == null && currentLifes > 0) {
			respawn ();
		}
	}

	void respawn(){
		currentLifes--;
		Instantiate();
	}

	public void Instantiate(){
		setControls ();

		instance = Instantiate(player,new Vector2(transform.position.x,transform.position.y),transform.rotation) as GameObject;
		instance.gameObject.SendMessage("setControls",keys);
	}

	public void setControls(){
		// Configurando os controles iniciais
		keys = new Dictionary<string,KeyCode> (){
			{"kcup",kcup},
			{"kcdown",kcdown},
			{"kcleft",kcleft},
			{"kcright",kcright},
			{"kcattack1",kcattack1},
			{"kcattack2",kcattack2},
			{"kcattack3",kcattack3},
		};
	}
}
