﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerNetwork : NetworkBehaviour {
	
	public float speed = 50.0f;
	public float jumpforce = 13.0f;
	public GameObject bullet;
	public GameObject crazyBullet;
	public GameObject ballisticBullet;
	public float totalhp = 50f;
	public float totalmp = 1000;
	public float regmp = 10;

	public KeyCode kcup = KeyCode.W;
	public KeyCode kcdown = KeyCode.S;
	public KeyCode kcleft = KeyCode.A;
	public KeyCode kcright = KeyCode.D;
	public KeyCode kcattack1 = KeyCode.H;
	public KeyCode kcattack2 = KeyCode.J;
	public KeyCode kcattack3 = KeyCode.K;

	public Dictionary<string,KeyCode> keys;
	
	bool jump = false;
	
	float hp;
	float mp;
	
	GameObject CBT;
	
	// Use this for initialization
	void Start () {
		// Configurando os controles iniciais
		keys = new Dictionary<string,KeyCode> (){
			{"kcup",kcup},
			{"kcdown",kcdown},
			{"kcleft",kcleft},
			{"kcright",kcright},
			{"kcattack1",kcattack1},
			{"kcattack2",kcattack2},
			{"kcattack3",kcattack3},
		};

		hp = totalhp;
		mp = totalmp;
		
		// Inicializando texto de dano
		CBT = transform.FindChild ("trianguloCanvas/CBT").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		Andar ();
		
		Pular ();
		
		Atirar ();
		
		Abaixar ();
		
		checkLife ();
		checkMana ();
	}
	
	// Corrige a orientação do canvas
	void correctCanvas(float mov){
		int coef = 0;
		if (mov < 0) {
			coef = -1;
		} else if(mov > 0){
			coef = 1;
		}
		
		if (coef != 0) {
			transform.Find ("trianguloCanvas").transform.localScale = 
				new Vector3(
					coef * Mathf.Abs(transform.Find ("trianguloCanvas").transform.localScale.x),
					transform.Find ("trianguloCanvas").transform.localScale.y,
					transform.Find ("trianguloCanvas").transform.localScale.z
					);
		}
	}
	
	// Verifica estado atual de saude
	void checkLife(){
		float lifepercentage;
		
		lifepercentage =  hp / totalhp;
		
		Color newcolor = new Color (1, lifepercentage, lifepercentage, 1);
		
		Color newcolorbar = new Color ((1-lifepercentage),(lifepercentage),0,1);
		
		// Alterando a cor do personagem
		GetComponent<SpriteRenderer> ().color = newcolor;
		transform.FindChild ("trianguloWeapon").gameObject.GetComponent<SpriteRenderer> ().color = newcolor;
		
		// Alterando a cor da barra de vida
		transform.Find("trianguloCanvas/hpbarBG/hpbarFG").gameObject.GetComponent<Image>().color = newcolorbar;
		
		// Diminuindo a largura da barra de vida
		transform.Find("trianguloCanvas/hpbarBG/hpbarFG").gameObject.GetComponent<Image>().fillAmount = lifepercentage;
		
		if (hp <= 0) {
			Destroy(gameObject);
		}
	}
	
	// Verifica o estado atual da mana
	void checkMana(){
		float manapercentage;
		
		// Corrigindo a mana atual conforme regeneração de mana
		if(mp < totalmp)
			mp += regmp * Time.deltaTime;
		
		if (mp > totalmp)
			mp = totalmp;
		
		manapercentage =  mp / totalmp;
		
		// Diminuindo a largura da barra de mana
		transform.Find("trianguloCanvas/mpbarBG/mpbarFG").gameObject.GetComponent<Image>().fillAmount = manapercentage;
	}
	
	// Faz perder vida
	void loseLife(float damage){
		float chance = Random.Range (0f, 1f);
		bool critical = false;
		
		if (chance < 0.3f) {
			damage *= 2;
			critical = true;
		}
		
		hp -= damage;
		
		string damageText = "- " + damage.ToString () + " hp";
		
		CBTInit (damageText,critical);
	}
	
	// Por enquanto só subtrai mana	
	void loseMana(float mana){
		mp -= mana;
	}
	
	
	// Inicializando texto de combate
	void CBTInit(string text,bool critical){
		GameObject temp = Instantiate (CBT) as GameObject;
		
		RectTransform tempRect = temp.GetComponent<RectTransform> ();
		temp.transform.SetParent (transform.FindChild ("trianguloCanvas"));
		
		tempRect.transform.localPosition = CBT.transform.localPosition;
		tempRect.transform.localScale = CBT.transform.localScale;
		tempRect.transform.localRotation = CBT.transform.localRotation;
		
		temp.SetActive (true);
		
		if(critical)
			temp.GetComponent<Animator> ().SetTrigger ("Crit");
		else
			temp.GetComponent<Animator> ().SetTrigger ("Hit");
		
		temp.GetComponent<Text> ().text = text;
		
		Destroy (temp, 2.0f);
	}
	
	// Altera os controles
	void setControls(Dictionary<string,KeyCode> vkeys){
		keys = vkeys;
	}
	
	
	// ---------- Ações -----------
	
	void Andar (){
		float mov = 0;

		if(isLocalPlayer){
			if (Input.GetKey (keys["kcright"])) {
				mov = 1;
			} else if (Input.GetKey (keys["kcleft"])) {
				mov = -1;
			}
			
			if (mov != 0) {
				transform.localScale = new Vector2(mov,transform.localScale.y);
			}
			
			// Corrige a orientação do canvas
			correctCanvas (mov);
			
			//transform.Translate
			
			Rigidbody2D rigidbody2d = GetComponent<Rigidbody2D> ();
			rigidbody2d.velocity = new Vector2((mov * speed),rigidbody2d.velocity.y);
		}
	}
	
	void Pular(){
		if (Input.GetKeyDown (keys["kcup"]) && !jump) {
			jump = true;
			
			GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpforce);
		}
	}
	
	void Atirar(){
		bool shot = false;
		GameObject instance = null;
		
		if (Input.GetKeyDown (keys["kcattack1"])) {
			instance = Instantiate(bullet,new Vector2(transform.position.x + (transform.localScale.x * 1.3f),transform.position.y),transform.rotation) as GameObject;
			bulletInit(instance);
		}
		
		if (Input.GetKeyDown (keys["kcattack2"])) {
			instance = Instantiate(crazyBullet,new Vector2(transform.position.x + (transform.localScale.x * 1.3f),transform.position.y),transform.rotation) as GameObject;
			bulletInit(instance);
		}
		
		if (Input.GetKeyDown (keys["kcattack3"])) {
			instance = Instantiate(ballisticBullet,new Vector2(transform.position.x + (transform.localScale.x * 1.3f),transform.position.y),transform.rotation) as GameObject;
			bulletInit(instance);
		}
	}
	
	void Abaixar(){
		if (Input.GetKey (KeyCode.DownArrow)) {
			transform.localScale = new Vector2 (transform.localScale.x, 0.5f);
		} else {
			transform.localScale = new Vector2 (transform.localScale.x, 1f);
		}
	}
	
	void bulletInit(GameObject instance){
		float cost = 0;
		
		cost = instance.GetComponent<Bullet> ().cost;
		
		if (cost <= mp) {
			loseMana (cost);
			instance.SendMessage ("setDirection", transform.localScale.x);
			transform.FindChild ("trianguloWeapon").GetComponent<Animator> ().SetTrigger ("Shoot");
		} else {
			Destroy (instance);
		}
	}
	
	void OnCollisionEnter2D(Collision2D coll){
		// Se cair no precipício, morre
		if (coll.gameObject.tag == "DeathPit") {
			loseLife(hp);
		}
		
		// Se estiver em uma plataforma, aponta que parou de pular
		if (coll.gameObject.layer == LayerMask.NameToLayer("Plataforma")) {
			jump = false;
		}
		
		// Se pular numa plataforma elevadiça, faz ele filho da plataforma para se moverem juntos
		if(coll.gameObject.tag == "Elevator"){
			transform.parent = coll.transform;
		}
	}
	
	void OnCollisionExit2D(Collision2D coll){
		if (coll.gameObject.tag == "Elevator") {
			transform.parent = null;
		}
	}
}
