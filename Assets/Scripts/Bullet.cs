using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float bulletspeed = 20f;

	public float damage = 5;

	protected int direction;

	protected float initialtime;

	protected float destroytime = 2f;

	public float cost = 100;

	// Use this for initialization
	void Start () {
		initialtime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if (initialtime + (destroytime * 1) < Time.time) {
			Destroy (gameObject);
		}

		moveBullet ();

//		if (height_up) {
//			if (height_value >= 1) {
//				height_up = false;
//			} else {
//				height_value += vertical_velocity;
//			}
//		} else {
//			if (height_value <= -1) {
//				height_up = true;
//			} else {
//				height_value -= vertical_velocity;
//			}
//		} 
	
//		Vector2 endPosition = new Vector2 (transform.position.x * 2, transform.position.y * 2);
//
//		gameObject.transform.position = Vector3.MoveTowards (transform.position, endPosition, (bulletforce * Time.deltaTime));

	}

	public virtual void moveBullet(){
		gameObject.transform.Translate (Vector2.right * (bulletspeed * Time.deltaTime * direction));
	}

	void setDirection(int i){
		direction = i;
	}

	void OnCollisionEnter2D(Collision2D coll){
		if (coll.gameObject.layer == LayerMask.NameToLayer("Plataforma")) {

		}

		if (coll.gameObject.tag == "Enemy") {
			coll.gameObject.SendMessage("loseLife",damage);
		}

		if (coll.gameObject.tag == "Player") {
			coll.gameObject.SendMessage("loseLife",damage);
		}

		// Uma bala não bate na outra
		if (coll.gameObject.tag != "Projectile") {
			Destroy (gameObject);
		}
	}
}
