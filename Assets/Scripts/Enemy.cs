﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

	public float totalhp = 15f;

	public float regeneration_value = 2f;

	public float regeneration_period = 0.5f;

	public float movement_period = 2f;

	public float shooting_period = 1f;

	public float jumpforce = 10f;

	public GameObject bullet;

	GameObject CBT;

	float hp;

	// Use this for initialization
	void Start () {
		hp = totalhp;

		// Inicializando regeneração de vida
		StartCoroutine(recoverLife ());

		// Inicializando movimentação
		StartCoroutine (defaultMovement ());

		// Inicializando tiro
		StartCoroutine (defaultAttack());

		// Inicializando texto de dano
		CBT = transform.FindChild ("inimigoCanvas/CBT").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		checkLife ();
	}

	// Verifica estado atual de saude
	void checkLife(){
		float newcolorfloat;
		
		newcolorfloat =  hp / totalhp;

		Color newcolor = new Color (1, newcolorfloat, newcolorfloat, 1);
		
		GetComponent<SpriteRenderer> ().color = newcolor;
		transform.FindChild ("inimigoWeapon").gameObject.GetComponent<SpriteRenderer> ().color = newcolor;
		if (hp <= 0) {
			Destroy(gameObject);
		}
	}

	// Faz inimigo perder vida
	void loseLife(float damage){
		float chance = Random.Range (0f, 1f);
		bool critical = false;

		if (chance < 0.3f) {
			damage *= 2;
			critical = true;
		}

		hp -= damage;

		string damageText = "- " + damage.ToString () + " hp";

		CBTInit (damageText,critical);
	}

	void OnCollisionEnter2D(Collision2D coll){

	}

	// Regeneração de vida
	IEnumerator recoverLife(){
		if (hp < totalhp) {
			hp += regeneration_value;
		}

		yield return new WaitForSeconds (regeneration_period);
		yield return StartCoroutine ("recoverLife");
	}

	// Inicializando texto de combate
	void CBTInit(string text,bool critical){
		GameObject temp = Instantiate (CBT) as GameObject;

		RectTransform tempRect = temp.GetComponent<RectTransform> ();
		temp.transform.SetParent (transform.FindChild ("inimigoCanvas"));

		tempRect.transform.localPosition = CBT.transform.localPosition;
		tempRect.transform.localScale = CBT.transform.localScale;
		tempRect.transform.localRotation = CBT.transform.localRotation;

		temp.SetActive (true);

		if(critical)
			temp.GetComponent<Animator> ().SetTrigger ("Crit");
		else
			temp.GetComponent<Animator> ().SetTrigger ("Hit");

		temp.GetComponent<Text> ().text = text;

		Destroy (temp, 2.0f);
	}

	// Movimentação padrão
	IEnumerator defaultMovement(){
		GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpforce);

		yield return new WaitForSeconds (movement_period);
		yield return StartCoroutine ("defaultMovement");
	}

	// Ataque padrão - Tiro
	IEnumerator defaultAttack(){
		GameObject instance = Instantiate(bullet,new Vector2(transform.position.x + (transform.localScale.x * 1.3f),transform.position.y),transform.rotation) as GameObject;
		// Alterando a cor do tiro
		instance.GetComponent<SpriteRenderer> ().color = new Color (0.5f, 1, 0.5f, 1);

		instance.SendMessage("setDirection",transform.localScale.x);

		yield return new WaitForSeconds (shooting_period);
		yield return StartCoroutine ("defaultAttack");
	}
}
