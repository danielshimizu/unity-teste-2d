﻿using UnityEngine;
using System.Collections;

public class PlatUpDown : MonoBehaviour {

	public float maxHeight = 10;

	public float velocity = 0.2f;

	// 1 sobe, -1 desce
	int direction = 1;

	float initialHeight;

	// Use this for initialization
	void Start () {
		// Armazenando altura inicial
		initialHeight = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		float currentHeight = transform.position.y;
		float diffHeight = currentHeight - initialHeight;

//		gameObject.transform.Translate (Vector2.up * (velocity * Time.deltaTime * direction));
		Rigidbody2D rigidbody2d = GetComponent<Rigidbody2D> ();
		rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x,(velocity * direction));

		if (direction == 1 && diffHeight >= maxHeight) {
			direction = -1;
		} else if(direction == -1 && currentHeight <= initialHeight){
			direction = 1;
		}
	}
}
