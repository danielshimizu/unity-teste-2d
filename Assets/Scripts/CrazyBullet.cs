﻿using UnityEngine;
using System.Collections;

public class CrazyBullet : Bullet {
	
	public float vertical_velocity = 20f;
	public float height_value = 0;
	public float crazy_cost = 120;

	float initial_height;
	bool height_up = true;

	void Awake(){
		cost = crazy_cost;
	}

	void Start(){
		initialtime = Time.time;
		initial_height = gameObject.transform.localPosition.y;
	}

	// Update is called once per frame
//	void Update () {
//		moveBullet ();
//	}
	
	public override void moveBullet(){
		float translate_height = 0;
		if (height_up) {
			translate_height = vertical_velocity * Time.deltaTime;
			
			if((gameObject.transform.localPosition.y - initial_height) >= height_value){
				height_up = false;
				
				translate_height = 2 + initial_height - gameObject.transform.localPosition.y;
			}
		} else {
			translate_height = vertical_velocity * Time.deltaTime * -1;
			
			if((gameObject.transform.localPosition.y - initial_height) <= 0){
				height_up = true;
				
				translate_height = initial_height - gameObject.transform.localPosition.y;
			}
		}

		gameObject.transform.Translate (Vector2.right * (bulletspeed * Time.deltaTime * direction));
		gameObject.transform.Translate (Vector2.up * translate_height);
	}
}
