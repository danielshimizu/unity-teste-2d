using UnityEngine;
using System.Collections;

public class Camera : MonoBehaviour {

	public float dampTime = 0.15f;
	private Vector3 velocity = Vector3.zero;
	public Transform[] target;

	public float minSize = 20f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// Criando um array com todos os objetos de jogador
		GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

		// Se houver apenas 1 jogador
		if (players.Length == 1) {
			Transform onlytarget = players [0].transform;
			Vector3 point = GetComponent<UnityEngine.Camera> ().WorldToViewportPoint (onlytarget.position);
			Vector3 delta = onlytarget.position - GetComponent<UnityEngine.Camera> ().ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
			Vector3 destination = transform.position + delta;
			transform.position = Vector3.SmoothDamp (transform.position, destination, ref velocity, dampTime);
		// Se houverem 2 jogadores
		} else if (players.Length == 2) {
			// Armazenando o transform de cada jogador
			Transform target1 = players[0].transform;
			Transform target2 = players[1].transform;

			// Calculando o ponto médio entre os dois jogadores
			float txMiddle = ( target1.position.x + target2.position.x ) / 2;
			float tyMiddle = ( target1.position.y + target2.position.y ) / 2;

			transform.position = new Vector3(txMiddle,tyMiddle,transform.position.z);
			float distance = 0;
			if(target1.position.x > target2.position.x){
				distance = Mathf.Abs(target1.position.x - target2.position.x) * 0.45f;
			}
			else{
				distance = Mathf.Abs(target2.position.x - target1.position.x) * 0.45f;
			}

			if(distance > minSize)
				GetComponent<UnityEngine.Camera>().orthographicSize = Mathf.Abs(distance);
			else
				GetComponent<UnityEngine.Camera>().orthographicSize = minSize;
		}
	}

	public void addTarget(Transform newtarget){
		target[target.Length] = newtarget;
	}
}
