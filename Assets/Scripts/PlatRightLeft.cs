﻿using UnityEngine;
using System.Collections;

public class PlatRightLeft : MonoBehaviour {

	public float velocity = 2f;

	public Transform currentPoint;

	public int pointSelection;

	public Transform[] points;
	
	// Use this for initialization
	void Start () {
		// Armazenando altura inicial
		currentPoint = points [pointSelection];
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.MoveTowards(transform.position,currentPoint.position,Time.deltaTime * velocity);

		if (transform.position == currentPoint.position) {
			pointSelection++;

			if(pointSelection == points.Length){
				pointSelection = 0;
			}

			currentPoint = points[pointSelection];
		}
	}}
