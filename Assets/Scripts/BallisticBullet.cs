﻿using UnityEngine;
using System.Collections;

public class BallisticBullet : Bullet {
	
	public float force = .001f;
	public float ballistic_cost = 150;

	void Awake(){
		cost = ballistic_cost;
	}

	void Start(){
		initialtime = Time.time;

		GetComponent<Rigidbody2D>().AddForce(Vector2.up * force,ForceMode2D.Impulse);
		GetComponent<Rigidbody2D>().AddForce(Vector2.right * 0.8f * force * direction,ForceMode2D.Impulse);
	}
	
	public override void moveBullet(){
//		float translate_height = 0;
//		if (height_up) {
//			translate_height = vertical_velocity * Time.deltaTime;
//			
//			if((gameObject.transform.localPosition.y - initial_height) >= height_value){
//				height_up = false;
//				
//				translate_height = 2 + initial_height - gameObject.transform.localPosition.y;
//			}
//		} else {
//			translate_height = vertical_velocity * Time.deltaTime * -1;
//			
//			if((gameObject.transform.localPosition.y - initial_height) <= 0){
//				height_up = true;
//				
//				translate_height = initial_height - gameObject.transform.localPosition.y;
//			}
//		}
//		
//		gameObject.transform.Translate (Vector2.right * (bulletspeed * Time.deltaTime * direction));
//		gameObject.transform.Translate (Vector2.up * translate_height);
//	}
	}
}